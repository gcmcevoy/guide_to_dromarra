\chapter{Religion}
\label{cha:religion}
Dromarra has, for as long as any stories can tell, been an island steeped in the druidic tradition. Although the role of and the significance of the druid has changed over the centuries, these figureheads have always represented two things for Inis Móna: a liaison to nature and, often, a conduit for the spiritual.

\begin{boxnote}{Playing a Druid}
    While many, if not most, druids are religious figures of varying import, it is not uncommon to see druids as adventurers in Dromarra. However, unlike in many other settings, the Dromarrach druid as a religious figure is not known to shapeshift. Given the island's fascination with lycanthropy and its mythology around transformation being something much more otherworldly, if not sinister, a druid's \emph{wild shape} should be used with caution, as it is seen as one of the more questionable things a druid can do.

    So, given the cultural significance placed on a druid, it is much more important to consider the social implications of an action than it would be elsewhere in the world. A druid who abuses certain powers or uses its bond with nature too atypically may find themselves rejected socially in a way that is more pronounced than even the more undesirable classes of spellcasters on the island.
\end{boxnote}

\begin{table*}[!h]
    \centering
    \begin{tabular}{ llll }
        \textbf{Name}   & \textbf{Rank} & \textbf{Symbol}       & \textbf{Portfolio} \\
        Ualal           & M             & The face in the sun   & The sun, the sky, punishment \\
        Márdowan        & M             & The stone slab        & Motherhood, tribal government \\
        Íoren           & I             & The yew branch        & Justice, cooperation, sacrifice, despair \\
        Galathnid       & L             &                       & The sea, immigrants \\
        Taraunish       & L             & The back of a fist    & War, dawn, healing, rebirth \\
        Dasclahoul      & L             & The cat with tongue   & Astrology, divination, injury, the human body \\
        Bálsiórud       & L             & The cupped hands      & The lost, the forgotten, the unwanted \\
    \end{tabular}
    \caption{The gods and goddesses of \emph{an phágántacht Dhromarrach}}
    \label{tab:gods_and_goddesses}
\end{table*}

\section{Na Déithe: The Gods and Goddesses}
\label{sec:gods_and_goddesses}
Dromarrach Paganism is practiced by the majority of natives, and it is the most influential of the most common religions on the island. Its practice dates back to antiquity, and it is one of the oldest surviving religions in the world. During the second great invasion, the Westerners attempted to eradicate \gls{paganism} by decimating holy sites, systematically eradicating holy men, and killing or enslaving its practitioners.

\subsection{Ualal}
\label{sub:ualal}\index{Ualal}
\trans{ualaíonn sé na réaltaí}{he holds down the stars}

\begin{figure}[H]
    \hangindent=0.7cm
    \emph{The Giver and Taker, The Burdener, Taker of Flames, The Divider } \\
    \textbf{Major Deity} \\
    \textbf{Symbol:} The face in the sun \\
    \textbf{Home Plane:} Tír na nDéithe \\
    \textbf{Portfolio:} The sun, the sky, punishment \\
    \textbf{Worshippers:} Commoners, reformed druids \\
    \textbf{Favored Weapon:} Greathammer \\
\end{figure}

Ualal is among the oldest of the gods. He, along with Márdowan, gave birth to the humans that inhabit Dromarra. Upon gazing at the first human and seeing the chaos her progeny would be capable of, he renounced his daughter - symbolically making humanity a bastard child of the gods.

After humans began to discover war, he tried to take fire back from them - forcing them to respect the old druidic ways. He did so without consulting the other gods and, ironically, inspired a war between the gods themselves when the act was deemed unjust by Íoren\index{Ioren@\'Ioren}, the very man that Ualal himself had deemed \gls{judge} of the gods.

After being upset in single combat by the once-human Íoren and forced to return the flame to humanity, Ualal became even more bitter towards his children.

\subsection{Márdowan}
\label{sub:mardowan}\index{Márdowan}
\trans{máthair an domhain}{mother of creation}

\begin{figure}[H]
    \hangindent=0.7cm
    \emph{The Mother} \\
    \textbf{Major Deity} \\
    \textbf{Symbol:} The stone slab \\
    \textbf{Home Plane:} Tír na nDéithe \\
    \textbf{Portfolio:} Motherhood, tribal government \\
    \textbf{Worshippers:} Mothers, clan leadership, patriots \\
    \textbf{Favored Weapon:} Shield \\
\end{figure}

Márdowan is the mother god, responsible for the creation of the Dromarrach island and its inhabitants. As one of the few gods that predates humanity, not much is known about her origin in \emph{Tír na nDéithe}. She remains hunched over, feet rooted at the floor of the sea, providing life and land for all of her children.

\subsection{Íoren}
\label{sub:ioren}\index{Ioren@\'Ioren}
\trans{fíor amháin}{the one true story}

\begin{figure}[H]
    \hangindent=0.7cm
    \emph{The Sword of Justice, The Mediator, High Brehon (Ard Breitheamh), The Sullen One, Sunken Eyes, The Voice of Man} \\
    \textbf{Intermediate Deity} \\
    \textbf{Symbol:} The yew branch \\
    \textbf{Home Plane:} Tír na nDoaine \\
    \textbf{Portfolio:} Justice, cooperation, sacrifice, despair \\
    \textbf{Worshippers:} Non-druids, \glspl{judge} \\
    \textbf{Favored Weapon:} Greatsword \\
\end{figure}

Íoren is the patron deity of the ever-evolving idea of justice. Born to a human mother and father, he was known as the only \gls{judge} with an advanced enough view on law to be trusted to govern the gods themselves. Among all of the gods, he has, throughout the cycles of myth, had the most direct interaction with humanity and is often believed to be residing in Dromarra at all times, acting as \gls{judge} for all humankind.

As a result, depictions of Íoren have changed over time. Over the centuries, other humans named Íoren (a somewhat popular name in Dromarra) have been believed to have been ascended into godhood to relieve the previous incarnation, giving him a complicated and subjective history.

\subsubsection{Followers of Íoren}
Fundamental to almost all followers of Íoren is that justice is subjective (hence, in part, the chaotic nature of him and his followers). This subjectivity is because law and order are not divine in nature - man is the judge, and man is inherently fallible. Zeal, therefore, can appear in a handful of ways. 

Some choose to focus on the subjectivity of law, life, and everything - these are more likely to be scholars and intellectuals and have a pretty absurdist view of things.

Others really focus on the idea of sacrifice - to make a just decision always comes at some cost, and that cost is often personal. The most ardent followers of this type will value sacrifice as a major form of showing one's devotion. This occasionally plays out as ascetisism or minimalism, but most of the more ardent followers of this type would be quick to criticize empty sacrifice as being selfish in its own way.

Another school of thought values justice as cooperative --- that humankind as a whole defines justice and individual subjectivity takes a backseat to the trends of society. These followers are typically more neutral than chaotic in alignment, as they value the advancement of mankind through a strict focus on specificity in law. These followers are more likely to try to advance in status, as they are more interested in changing the way humankind perceives law, as the power of an individual \gls{judge} is much more limited. They also tend towards good, rather than neutrality, as they are very concerned with the betterment of mankind.

For a warrior, things might play out slightly differently. Some warriors see themselves as arbiters of justice. The \gls{judge} is the judge/jury, but not the ``executioner.'' These followers are among the most likely to trend chaotic. The subjectivity involved in a \gls{judge}'s decision is then filtered through the subjectivity of the warrior. Many others will disagree with this approach, as it is far more likely to end in death for the guilty, and capital punishment is \emph{very} rare in society.

Others will position themselves as ``defenders,'' in a sense. They are dedicated to preserving the sovereignty of Dromarra, or mankind as a whole. They tend towards xenophobia, as they value \gls{judge} law as a system over all else.

\subsection{Galathnid}
\label{sub:galathnid}\index{Galathnid}
\trans{gialla an ruda anaithnid}{jaws of the unknown thing}
\begin{figure}[H]
    \hangindent=0.7cm
    \textbf{Lesser Diety} \\
    \textbf{Symbol:} \\
    \textbf{Home Plane:} Tír na nDoaine \\
    \textbf{Portfolio:} The sea, immigrants \\
    \textbf{Worshippers:} Immigrants, fishermen, sailors \\
    \textbf{Favored Weapon:} Shortspear \\
\end{figure}

Galathnid is the patron deity of the sea.

\subsection{Taraunish}
\label{sub:taraunish}\index{Taraunish}
\trans{tharraing sí}{she tore}
\begin{figure}[H]
    \hangindent=0.7cm
    \emph{The Valkyrie, The Shield of Dawn, The Undying} \\
    \textbf{Lesser Diety} \\
    \textbf{Symbol:} The back of a fist \\
    \textbf{Home Plane:} Tír na nDoaine \\
    \textbf{Portfolio:} War, dawn, healing, rebirth \\
    \textbf{Worshippers:} Warriors, women, clan leaders \\
    \textbf{Favored Weapon:} Greatsword \\
\end{figure}

The goddess of war. The youngest of all the gods, she is believed to be a Northman who began to secretly practice Págánacht na nDraoithe. She was discovered and declared outlaw by the Northmen and was slain by a group of Northmen shortly after. Her death was first written of in 257aa by the Northman Invasion scholar Ceilg-Mhian\index{Ceilg-Mhian} in The Legacy of An Críoch Réanna: A Study of the Branching Influence of the First Great Invasion. Ceilg-Mhian, despite his well-known skepticism of certain embellishments of the oral tradition, faithfully recorded the following story, as told to him by the druid Tacaí Gcrann\index{Druids!Tacaí Gcrann}:

\begin{quote}
    \emph{Mogh Cré\index{Druids!Mogh Cré}, watching uphill, felt the earth beneath his feet turn damp with the blood of the Northmen. Unaffected by the greataxe she had taken to the stomach, Taraunish soared into a ríastrad the likes of which he had never before been seen. Limbs began getting tangled in the branches of trees around the outcropping. After dispatching over a hundred of the largest warriors the Northmen had to offer, she finally fell to her knees, axes in hand, with so many arrows and weapons protruding from her skin that her shape was barely recognizable as human. Twenty men twice her size were surrounding her, with nearly as many hiding behind them, afraid to encroach any farther into the space she had won for herself. After nearly a day in this standstill, a single crow landed on Taraunish. When it began to peck at her, they knew it was done. They moved to take her body with them as a prize for their efforts, but she turned into soil at their touch and disappeared into the earth beneath them.}
\end{quote}

Ceilg-Mhian also notes that, ``unsurprisingly, no written or artistic references to the goddess appear before this period [of The Great Invasion] - lending credence to the idea that 1) her ‘ascension’ into godhood was a product of the Isle’s desperation for a talisman of resistance, and 2) although far less likely, she was an actual human being who lived around this time.''

\subsection{Dasclahoul}
\label{sub:dasclahoul}\index{Dasclahoul}
\trans{dhathaigh sé an scliúchas stairiúil}{he colored the storied brawl}
\begin{figure}[H]
    \hangindent=0.7cm
    \emph{The Unspoken, The Puppeteer, Boiler of Blood, The Imbuer, The Illuminator, The One Who Painted Man} \\
    \textbf{Lesser Diety} \\
    \textbf{Symbol:} The cat with tongue \\
    \textbf{Home Plane:} Tír na nDoaine \\
    \textbf{Portfolio:} Astrology, divination, injury, the human body \\
    \textbf{Worshippers:} The crippled, scholars, skeptics, non-druid mages \\
    \textbf{Favored Weapon:} Knife \\
\end{figure}
Dasclahoul was an ancient druid who dabbled in wizardry and sorcecy - kinds of magic especially frowned upon by ancient \glspl{druid}. Living life as an outcast, when accused of a crime, instead of having the typical fine levied against him, his tongue was ordered to be cut out - although the exact nature of his crime has been lost to history, as this was well before written record and Dasclahoul, for obvious reasons, was not able to speak of it. This disfiguration sent him spiraling farther down the rabbit hole of conspiracy theory and what one might consider ``mad science.'' In an attempt to overcome the handicaps forced upon him, he fell deeper into his studies, eventually becoming so well-versed in both ancient and contemporary magic and artifice that he restored his body to far better condition. This is how he ascended into godhood, where he sits to this day.

Along with Íoren, Dasclahoul is among the most frequently-rumored of the gods to appear to humans, offering them knowledge for the sake of disrupting the status quo or revealing a hint at a greater conspiracy. These stories are rarely believed, as many criminals and many lunatics cite visits from Dasclahoul as an excuse for their actions.

Importantly, Dasclahoul's relationship with Íoren is that of a strong mutual respect and, some believe, friendship. In fact, Dasclahoul's \gls{nickname} comes from a story in which he gave Íoren\index{Ioren@\'Ioren} a magical tattoo, allowing him to best the greater god Ualal\index{Ualal} in battle, in order to bring him to justice after he attempted to take the flame back from humanity.

\subsection{Bálsiórud}
\label{sub:balsiorud}\index{Bálsiórud}
\trans{ba mhaith leis an rud}{the thing wants}
\begin{figure}[H]
	\hangindent=0.7cm
    \emph{The Unnatural, Not-Quite Man, Born to None, The Patient One, Keeper of the Unwanted} \\
	\textbf{Lesser Diety} \\
	\textbf{Symbol:} The cupped hands \\
	\textbf{Home Plane:} Tír na nDoaine \\
    \textbf{Portfolio:} The lost, the forgotten, the unwanted \\
    \textbf{Worshippers:} The homeless, tinkerers, collectors, orphans, kinless \\
	\textbf{Favored Weapon:} Hammer \\
\end{figure}
Bálsiórud was created as a life-size puppet meant to entertain the orphans of Dromarra. His creator, Déantáis, was a brilliant craftsman with an incredibly charitable heart. He was known across the land for his generosity in fostering and adopting children. When he became ill and began to see that his days were swiftly coming to an end, he dedicated the remainder of his life to turning his prized creation into something more. He wanted to give literal life to his creation.

He consulted scholars, healers, druids, and attempted to reach the gods themselves in his journey to finish his masterpiece. Finally, as he lay dying, Bálsiórud moved on his own for the first time. Some say it was the mother herself that took pity on old Déantáis. Others say it was Dasclahoul, who saw the novelty in the idea. Still others believe that it was truly the diligence and the genius of Déantáis that created the first truly animatronic man. No matter the source, Bálsiórud was granted the blessing of life - although for him, that blessing would quickly turn into a curse.

Over the years, fascination with the creation brought hundred upon hundreds of unwanted visitors to the orphanage. Some came to marvel, others to offer blessings, but some others came to vandalize and terrorize. After he awoke one night to the orphanage ablaze and masked vandals tying him down and carving into his shell, Bálsiórud erupted in anger, killing the intruders. Without the orphanage, Bálsiórud retired to the forest, where his existence would cause no more pain.

As the years went by, and those who had witnessed his improbable life firsthand began to pass, the truth of his existence became less and less commonly accepted. It is said that as the final memory of his tale faded, Íoren\index{Ioren@\'Ioren} saw the injustice Bálsiórud had suffered - to be brought into life without any of the humanity afforded those born by conventional means. He had no kin, he had no ambition, no death coming for him, and no say in any of it.

Thus, Bálsiórud was given purpose, as he once had. He sits among the gods, capturing anything that slips between the worlds of the gods, man, and nothingness. All things truly forgotten fall to him, and he cares for them as he once cared for Déantáis and his children.
