\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Inis M\IeC {\'o}na, or The Isle of Peat}{4}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}The Origins of Life}{4}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}A Brief History of Dromarrach Society}{4}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Dromarra Today}{5}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Culture}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Calendar}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Gender}{6}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Gender in inheritance}{6}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Gender in war}{6}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Family}{7}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Naming}{7}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Architecture}{7}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Class Structure}{7}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Appearance \& Clothing}{7}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}Race}{7}{subsection.2.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Societies}{8}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.1}The Fil\IeC {\'\i }}{8}{subsection.2.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.2}The Canonical Druidic Order}{8}{subsection.2.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.3}The Cult of the Severed Head}{8}{subsection.2.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.4}The Pugilists}{8}{subsection.2.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.5}\acrlong {dro}}{8}{subsection.2.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Songs of \acrlong {dro}}{8}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Our Time Will Come}{8}{section*.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.6}The Ministry of the Astral Gate}{8}{subsection.2.7.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Law}{9}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}The Basis of Law}{9}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Religion}{10}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Na D\IeC {\'e}ithe: The Gods and Goddesses}{10}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Ualal}{10}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}M\IeC {\'a}rdowan}{11}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}\IeC {\'I}oren}{11}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Followers of \IeC {\'I}oren}{12}{section*.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}Galathnid}{12}{subsection.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}Taraunish}{12}{subsection.4.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.6}Dasclahoul}{13}{subsection.4.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.7}B\IeC {\'a}lsi\IeC {\'o}rud}{13}{subsection.4.1.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Politics}{15}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Clan Organization}{15}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}The Petty Kings and Queens}{15}{subsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}The Overkings and Overqueens}{15}{subsection.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}The Provincial Kings and Queens}{15}{subsection.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}The High Kings and Queens}{16}{subsection.5.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Selecting the \gls {high king}}{16}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{External Proclamation}{16}{section*.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{The Tanist Stone}{16}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Notable Exceptions}{16}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Na Poic F\IeC {\'e}as\IeC {\'o}gach}{16}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.5}Court Life}{17}{subsection.5.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{The \emph {Aithech Fortha}}{17}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{The \emph {\IeC {\'A}gae Fine}}{17}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}The Major Clans}{17}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Wintermane}{17}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Notable Members}{17}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Elmsborn}{17}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.3}W\IeC {\'o}dblood}{17}{subsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Minor Clans}{17}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Fe\IeC {\'a}}{17}{section*.13}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Geography}{19}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Places}{19}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.1}Territories of Clan Wintermane}{19}{subsection.6.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Culneary (from \emph {C\IeC {\'u}l na n-Ionr\IeC {\'o}ir\IeC {\'\i }} or ``backs of the invaders'')}{19}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Oldchurch, a.k.a. D\IeC {\'u}nl\IeC {\'e}inn (modernized from \emph {} or ``the fort of civilization'')}{19}{section*.15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Balbasht\IeC {\'\i } (from \emph {Baile na B\IeC {\'a}ist\IeC {\'\i }} or ``home of the rains'')}{19}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Cnoc Sinsear (from \emph {Cnoc Sinsear} or ``Hill of the Progenitors'')}{19}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Cillmhaighnean}{19}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sos-h-\IeC {\'I}oren}{19}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Rathgorclave}{20}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{D\IeC {\'u}nshaghlin}{20}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.2}Territories of Clan W\IeC {\'o}dblood}{20}{subsection.6.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Colkirk}{20}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Wolf Hill}{20}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Ballysagart}{20}{section*.24}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Scarnagh}{20}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Halfway}{20}{section*.26}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Laytown}{20}{section*.27}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Mountkenne}{20}{section*.28}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1.3}Territories of Clan Elmsborn}{20}{subsection.6.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Rough Glasne}{20}{section*.29}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Cilltiarn\IeC {\'a}n}{20}{section*.30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Laghale \IeC {\'A}rda}{20}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Sligory}{20}{section*.32}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Ballygolen}{20}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Talbogan (from \emph {Talamh an Bheag\IeC {\'a}n} or ``land of the few'')}{20}{section*.34}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Glossary}{22}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acronyms}{23}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Index}{24}{chapter*.38}
